## This has been migrated to GitHub! https://github.com/uri-cloudops/uri-ansible

You need to download some secret files to use this playbook! Run the
`get-secrets.sh` script to download them.

To deploy this playbook:
```bash
ansible-playbook -i inventory.ini main.yml --vault-password-file secrets/vault_pass.txt
```

Some files are encrypted at rest with Ansible vault, to view or edit these files
run:
```bash
ansible-vault edit roles/wiki/vars/main.yml --vault-password-file secrets/vault_pass.txt
```

To login to the wiki server (or any server in the inventory file)
```bash
ssh -i secrets/ssh_uriux uriux@wiki.uriuxlabs.com
```
