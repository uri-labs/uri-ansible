#!/bin/bash

# Store the account the user is currently logged in as
currentaccount=$(gcloud config list --format json | jq -r '.core.account')
currentproject=$(gcloud config list --format json | jq -r '.core.project')

finish() {
  rm -vf secrets/secrets.tar.gz
  echo "Resetting gcloud credentials..."
  gcloud config set core/account "$currentaccount"
  gcloud config set core/project "$currentproject"
}

trap finish EXIT

# List the accounts that we have access to
echo "Accounts you have credentials for:"
accounts=$(gcloud auth list --format json | jq -r '.[].account')
i=0
for acct in $accounts; do
  ((i++))
  echo " $i: $acct"
done
echo "Please type the number of the account you want to use:"
echo "It needs have access to the study_secrets bucket in the uri-cloud-support project!"
read -r -p "Account number: " accountIndex

# Get the account at index i
account="NULL"
i=0
for acct in $accounts; do
  ((i++))
  if [ "$i" == "$accountIndex" ]; then
    account=$acct
    break;
  fi
done

if [ "$account" == "NULL" ]; then
  echo "Can't find account at index $i!!!"
  exit -2
fi

mkdir -p "secrets"

echo "Setting credentials for $account..."
gcloud config set core/account "$account"
gcloud config set core/project uri-cloud-support
gsutil cp gs://study_secrets/uriuxinfra-secrets.tar.gz secrets/secrets.tar.gz

tar xvf secrets/secrets.tar.gz -C secrets

# Restrict permissions
find secrets -type f | xargs chmod -v 0600
find secrets -type d | xargs chmod -v 0700
