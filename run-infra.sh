#!/bin/bash
# This is meant to be run from the URIUX ansible server, on a systemd timer
set -e
cd "$(dirname "$0")" || exit

git fetch
git checkout origin/master

ansible-playbook -i inventory.ini site.yml --vault-password-file secrets/vault_pass.txt
